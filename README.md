
# Welcome to the repository for Useful Tools  
Hi! I'm Brett Garver, the sole Proprietor, Creator, and Beneficiary of these life altering tools. You may have already had the pleasure of using my creations. Now you will be able to see behind the curtain of glory.

Be careful with her, she may be fidgety, as she's BETA testing.  
  
*See below for a list of wonderful tools*  
  
## Time Calculator  
This tool will allow the beholder to track their time with this company. The following situations apply:

 - Counting OT so you can be "Livin' La Vida Loca"
 - Milking the company for every penny of your worth
 - Workin' the Bear Necessities so you don't get canned
 - See how many hours until you don't have to listen to the boss say "That'd be great" for two days.

Regardless of your reasoning, the only reason you're even still reading this, is because you like to see them gears turn and hear that engine rev. Well this one likes to be told what to do via:

 - Manual Input
	 - This allows the user to go through Sunday - Saturday and input their clock in / out times manually. AS MANY AS THEY DESIRE! It's a free for all! Go ahead, clock in / out 30 times, it doesn't mind.

This, however, is not the only reason for its existence. It also has a feature that will calculate the Moolah that you have acquired throughout the week. For those who may have fallen on hard times, the amount they may have let twirl around for too long before it disappeared.

## Shift Finder
This tool grants the user abilities of futuristic sight. In regard to their work schedule that is.

The primary audience is the Help Desk which currently used a TUES-SAT, MON-FRI, SUN-THUR schedule

By selecting:

 - TUES - SAT
 - MON - FRI
 - SUN - THUR
 
 This will allow you to scroll through a calendar and see the days on and off that month to better allow planning your mischievous events.
 
## De-Obfuscator
This tool allows the user to achieve level 100 sight seeing.

Sometimes in this world we are unaware if the computer is showing a 1 or l, 0 or o, i or l.

This is where the De-Obfuscation tool comes in handy. 

By pasting/typing your text into the tool and analyzing it, you are able to get clarity on these questions.

#Changelog
##1.4.0
 - Added the ability for the spring server to filter out good and bad requests
 - If too many requests to the same url are made in one day, it's blocked
 - If too many redirects to error are made in one day, it's blocked
 - All other requests will be documented
 - A Summary email will be sent daily to my email.
##1.3.0
 - Added Spring Security to the application.
 - Incorporated a Dark Mode
 - Created a profile page to make adjustments to your account
 - Remove use of cookies in Time Calculator  
 - Also stores user settings in database for Shift finder and dark mode if you choose to make an account.
 - Spring security is an essential piece that helps me towards the next feature of review submissions.
##1.2.3
 - Fixed some issues with the modal not appearing on time calculator.
 - Fixed some special characters being recognized as capitols in de-obfuscator.
 - updated the fullcalendar to version 5.5 (this is what was causing issues with the modal)
 - cleaned up the scripts and links, so I'm not loading unnecessary documents on pages that don't need it.
 - Updated the wording on the Time Calculator modal, because it discussed functions that no longer exist.
###1.2.2
 - Made some changes to the attribute names.
 - Added Cookie validation. This will reset the cookies and reload the page if tampering was detected.
 - Fixed an issue with the De-Obfuscator where long "words" wouldn't scroll or wrap.
###1.2.1
 - Fixed the tooltips not working on every page.
###1.2.0
 - Added the De-Obfuscation tool to the site. Also altered the homepage reviews.
###1.1.1
 - Squashed a bug that caused the Shift Finder to not display. This was caused by the way the calendar class processes the week of year. The week that 1/1 falls on is week #1 even if it's 12/31 it's still week 1.
 - Squashed another bug that caused the wrong day numbers to be displayed in the time calculator. This was caused by UTC to CST time adjustments occurring 2+ times.