package com.thegarver.usefultools.service;

import com.thegarver.usefultools.dao.IpAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Enumeration;

@Service
public class RequestService {
    private static Logger log = LoggerFactory.getLogger(RequestService.class);

    private IpAccessService ipAccessService;

    @Autowired
    public RequestService(IpAccessService ipAccessService) {
        this.ipAccessService = ipAccessService;
    }


    public void addIpToServerFirewall(HttpServletRequest request, String reason) {
        String ipAddress = getRemoteAddr(request);

        log.error("Blocked " + ipAddress + " due to " + reason);
        String precedent = "";

        if (request.getMethod().equalsIgnoreCase("POST")) {
            precedent = "POST Blocked | ";
        } else {
            precedent = "Blocked | ";
        }

        IpAccess ipAccess = new IpAccess();

        ipAccess.setError(true);
        ipAccess.setFullUrl(precedent + getRealUrl(request));
        ipAccess.setDateTime(new Date());
        ipAccess.setIpAddress(ipAddress);

        ipAccessService.addNewIpAccess(ipAccess);
        try {
            String[] args = new String[]{"/bin/bash", "-c", "sudo ufw insert 2 deny from " + ipAddress + " to any"};
            Process process = new ProcessBuilder(args).start();
            InputStreamReader isr = new InputStreamReader(process.getInputStream());
            BufferedReader br = new BufferedReader(isr);

            StringBuilder stringBuilder = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            log.info(stringBuilder.toString());

            isr = new InputStreamReader(process.getErrorStream());
            br = new BufferedReader(isr);
            stringBuilder = new StringBuilder();
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
            if (stringBuilder.length() > 0) {
                log.error(stringBuilder.toString());
            }
        } catch (IOException ioe) {
            log.error(ioe.getMessage());
        }
    }

    public String getRemoteAddr(HttpServletRequest request) {
        String ipFromHeader = request.getHeader("X-FORWARDED-FOR");
        if (ipFromHeader != null && ipFromHeader.length() > 0) {
            log.debug("ip from proxy - X-FORWARDED-FOR : " + ipFromHeader);
            return ipFromHeader;
        }
        return request.getRemoteAddr();
    }

    public String getParameters(HttpServletRequest request) {
        StringBuffer posted = new StringBuffer();
        Enumeration<?> e = request.getParameterNames();
        if (e != null) {
            posted.append("?");
        }
        while (e.hasMoreElements()) {
            if (posted.length() > 1) {
                posted.append(" and ");
            }
            String curr = (String) e.nextElement();
            posted.append(curr + "=");
            if (curr.contains("password")
                    || curr.contains("pass")
                    || curr.contains("pwd")) {
                posted.append("*****");
            } else {
                posted.append(request.getParameter(curr));
            }
        }
        return posted.toString();
    }

    public String getRealUrl(HttpServletRequest request) {
        String[] providedPath = request.getRequestURL().toString().split("/");
        String originalPath = (String) request.getAttribute("javax.servlet.forward.request_uri");
        //if (originalPath == null) {
        //    List<String> attributes = Collections.list(request.getAttributeNames());
        //    for (String item : attributes) {
        //        System.out.println("Name: " + item + " | Value: " + request.getAttribute(item));
        //    }
        //}
        String createdPath = "";
        if (providedPath.length >= 3) {
            createdPath += providedPath[0];
            createdPath += "//";
            createdPath += providedPath[2];
            createdPath += originalPath;
            for (int i = 4; i < providedPath.length; i++) {
                createdPath += "/" + providedPath[i];
            }
            createdPath += getParameters(request);
        }
        if (createdPath.contains("null")) return request.getRequestURL().toString() + getParameters(request);
        return createdPath;
    }
}
