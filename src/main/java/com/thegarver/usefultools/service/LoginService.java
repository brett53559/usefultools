package com.thegarver.usefultools.service;

import com.thegarver.usefultools.dao.Login;
import com.thegarver.usefultools.dao.repository.LoginRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class LoginService {

    @Autowired
    protected LoginRepository loginRepository;

    public Optional<Login> findLoginByEmail(String email) {
        return loginRepository.findByEmail(email);
    }

    public void createNewLogin(Login login) {
        loginRepository.save(login);
    }

    public boolean doesEmailExist(String email) {
        Optional<Login> login = loginRepository.findByEmail(email);

        return login.isPresent();
    }

    public Optional<Login> findLoginFromActivationId(String activationId) {
        return loginRepository.findByActivationIdLikeAndActivationExpirationAfter(activationId, DateTime.now().toDate());
    }

    public Optional<Login> findLoginFromResetId(String resetId) {
        return loginRepository.findByResetIdLikeAndResetExpirationAfter(resetId, DateTime.now().toDate());
    }

    public void saveLogin(Login login) {
        loginRepository.save(login);
    }
}
