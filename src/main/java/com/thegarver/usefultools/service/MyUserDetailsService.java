package com.thegarver.usefultools.service;

import com.thegarver.usefultools.dao.Login;
import com.thegarver.usefultools.dao.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    LoginService loginService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Login> login = loginService.findLoginByEmail(email);

        login.orElseThrow(() -> new UsernameNotFoundException(email + " not found"));

        return login.map(MyUserDetails::new).get();
    }
}
