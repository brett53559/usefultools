package com.thegarver.usefultools.service;

import com.thegarver.usefultools.dao.Login;
import com.thegarver.usefultools.dao.UserSettings;
import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.dto.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.stereotype.Service;

@Service
public class ProfileService {

    UserSettingsService userSettingsService;
    LoginService loginService;
    UsersService usersService;

    @Autowired
    public ProfileService(UserSettingsService userSettingsService, LoginService loginService, UsersService usersService) {
        this.userSettingsService = userSettingsService;
        this.loginService = loginService;
        this.usersService = usersService;
    }

    public UserInfo getUserInfo(String email) {
        Login login = loginService.findLoginByEmail(email).get();
        Users users = usersService.findUserFromEmail(email);

        return new UserInfo(users.getFirstName(), users.getLastName(), login.getEmail(), "");
    }

    public boolean updateEmail(String currentEmail, String newEmail) {

        if (!loginService.doesEmailExist(newEmail)) {
            Login login = loginService.findLoginByEmail(currentEmail).get();

            login.setEmail(newEmail);
            loginService.saveLogin(login);
            return true;
        }
        return false;
    }

    public void updateName(String firstName, String lastName, String email) {
        Users users = usersService.findUserFromEmail(email);

        users.setFirstName(firstName);
        users.setLastName(lastName);

        usersService.saveUser(users);
    }

    public void updateDarkMode(String email, boolean darkModeEnabled) {
        Users users = usersService.findUserFromEmail(email);

        userSettingsService.updateDarkMode(users, darkModeEnabled);
    }

    public boolean passwordIsValid(String password, String email) {
        Login login = loginService.findLoginByEmail(email).get();

        return PasswordEncoderFactories.createDelegatingPasswordEncoder().matches(password, login.getPassHash());
    }

    public void updatePassword(String password, String email) {
        Login login = loginService.findLoginByEmail(email).get();

        login.setPassHash(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(password));

        loginService.saveLogin(login);
    }

    public void removeProfile(String email) {
        usersService.removeProfile(email);
    }
}
