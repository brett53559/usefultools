package com.thegarver.usefultools.service;

import com.thegarver.usefultools.dao.IpAccess;
import com.thegarver.usefultools.dao.repository.IpAccessRepository;
import com.thegarver.usefultools.util.DateUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class IpAccessService {

    protected IpAccessRepository ipAccessRepository;

    @Autowired
    public IpAccessService(IpAccessRepository ipAccessRepository) {
        this.ipAccessRepository = ipAccessRepository;
    }

    public boolean doesIpAddressExist(String ipAddress) {
        Optional<IpAccess> ipResult = ipAccessRepository.findDistinctFirstByIpAddress(ipAddress);

        return ipResult.isPresent();
    }

    public void addNewIpAccess(IpAccess ipAccess) {
        ipAccessRepository.save(ipAccess);
    }

    public int withinSecondCount(String ipAddress) {

        long currentTime = new Date().getTime();
        List<IpAccess> ipAccessList = ipAccessRepository.findByIpAddressLikeOrderByIdDesc(ipAddress);

        int withinSecondCount = 0;
        for (IpAccess ipAccess : ipAccessList) {
            long difference = currentTime - ipAccess.getDateTime().getTime();
            if (difference < 1000) {
                withinSecondCount++;
            }
        }
        return withinSecondCount;
    }

    public List<IpAccess> getDailySummary() {
        DateUtility dateUtility = new DateUtility(new Date(), true, false);

        return ipAccessRepository.findAllByDateTimeAfter(dateUtility.getCurrentDate());
    }
}
