package com.thegarver.usefultools.service;

import com.thegarver.usefultools.dao.TcTimes;
import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.dao.repository.TcTimesRepository;
import com.thegarver.usefultools.util.DateUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TcTimeService {
    public static final int DEFAULT_HOURS = 40;
    public static final double DEFAULT_WAGE = 15.0;

    @Autowired
    protected TcTimesRepository tcTimesRepository;

    protected DateUtility dateUtility;

    public List<TcTimes> getAllTimesForID(Users users) {
        return tcTimesRepository.findAllByUsersOrderByDayDescShiftCountDesc(users);
    }

    public void addTimes(Users users, int shiftCount, int day, Date clockIn, Date clockOut) {

        TcTimes tcTimes = new TcTimes(users, day, shiftCount, clockIn, clockOut);
        tcTimesRepository.save(tcTimes);
    }

    public void deleteUserRecords(Users userID) {
        tcTimesRepository.deleteAllById(userID.getId());
    }
}
