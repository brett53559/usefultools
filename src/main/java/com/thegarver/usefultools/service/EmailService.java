package com.thegarver.usefultools.service;

import com.thegarver.usefultools.configuration.EmailConfiguration;
import com.thegarver.usefultools.dao.IpAccess;
import com.thegarver.usefultools.dto.EmailInline;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class EmailService {

    private static final String SENDER_ADDRESS = "brett@thegarver.com";

    private static final String WELCOME_EMAIL_TEMPLATE_CLASSPATH = "classpath:static/email/welcomeemail.html";
    private static final String PASS_RESET_EMAIL_TEMPLATE_CLASSPATH = "classpath:static/email/passreset.html";

    private static final String RON_WELCOME_IMAGE = "static/IMG/ron_welcome.jpg";
    private static final String PASS_RESET_IMAGE = "static/IMG/pass_reset.jpg";
    private static final String LOGO_IMAGE = "static/IMG/UsefulToolsLogo.png";

    private static final String CONTEXT_NAME = "name";
    private static final String CONTEXT_GENERATED_URL = "generatedurl";
    private static final String CONTEXT_EMAIL = "myemail";

    @Value("${pageLinks.register}")
    private String REGISTRATION_PAGE;

    @Value("${pageLinks.passreset}")
    private String PASS_RESET_PAGE;

    private static final String TEXT_ENCODING = "UTF-8";

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private JavaMailSender mailSender;

    @Qualifier("emailTemplateEngine")
    @Autowired
    private TemplateEngine stringTemplateEngine;

    @Autowired
    private IpAccessService ipAccessService;

    private boolean sendTextMail(
            final String subject, final String recipientEmail, final String body)
            throws MessagingException {

        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, TEXT_ENCODING);
        message.setSubject(subject);
        message.setFrom(SENDER_ADDRESS);
        message.setTo(recipientEmail);

        message.setText(body);

        // Send email
        this.mailSender.send(mimeMessage);
        return true;
    }

    private boolean sendEditableMail(
            String recipientEmail,
            String subject,
            List<String[]> contextVariables,
            List<EmailInline> inlineList,
            int emailType)
            throws MessagingException {

        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, TEXT_ENCODING);
        message.setSubject(subject);
        message.setFrom(SENDER_ADDRESS);
        message.setTo(recipientEmail);

        // Prepare the evaluation context
        final Context ctx = new Context();
        for (String[] contextVariable : contextVariables) {
            ctx.setVariable(contextVariable[0], contextVariable[1]);
        }

        String htmlContent = "";
        try {
            switch (emailType) {
                case 1:
                    //Welcome Email
                    htmlContent = resolveTemplate(WELCOME_EMAIL_TEMPLATE_CLASSPATH);
                    break;
                case 2:
                    htmlContent = resolveTemplate(PASS_RESET_EMAIL_TEMPLATE_CLASSPATH);
                    break;
            }
        } catch (IOException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            return false;
        }

        // Create the HTML body using Thymeleaf
        final String output = stringTemplateEngine.process(htmlContent, ctx);
        message.setText(output, true);

        if (inlineList != null) {
            for (EmailInline emailInline : inlineList) {
                // Add the inline images, referenced from the HTML code as "cid:image-name"
                message.addInline(emailInline.getContentId(), emailInline.getInputStreamSource(), emailInline.getContentType());
            }
        }

        // Send mail
        this.mailSender.send(mimeMessage);
        return true;
    }

    public boolean sendWelcomeEmail(String recipientEmail, String name, String activationId) {
        String generatedUrl = GlobalStaticVars.DOMAIN_NAME + "/" + REGISTRATION_PAGE + "/activate?" + GlobalStaticVars.URL_PARAM_ACTIVATIONID + "=" + activationId;
        List<String[]> contextVariables = new ArrayList<>();
        contextVariables.add(new String[]{CONTEXT_NAME, name});
        contextVariables.add(new String[]{CONTEXT_GENERATED_URL, generatedUrl});
        contextVariables.add(new String[]{CONTEXT_EMAIL, SENDER_ADDRESS});

        List<EmailInline> inlineList = new ArrayList<>();
        inlineList.add(new EmailInline("usefultools-logo", new ClassPathResource(LOGO_IMAGE), EmailInline.PNG_MIME));
        inlineList.add(new EmailInline("ron_welcome", new ClassPathResource(RON_WELCOME_IMAGE), EmailInline.JPG_MIME));

        try {
            return sendEditableMail(recipientEmail,
                    "Welcome to Useful Tools",
                    contextVariables,
                    inlineList,
                    1);
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean sendPassResetEmail(String recipientEmail, String name, String resetId) {
        String generatedUrl = GlobalStaticVars.DOMAIN_NAME + "/" + PASS_RESET_PAGE + "?" + GlobalStaticVars.URL_PARAM_PASSRESETID + "=" + resetId;
        List<String[]> contextVariables = new ArrayList<>();
        contextVariables.add(new String[]{CONTEXT_NAME, name});
        contextVariables.add(new String[]{CONTEXT_GENERATED_URL, generatedUrl});
        contextVariables.add(new String[]{CONTEXT_EMAIL, SENDER_ADDRESS});

        List<EmailInline> inlineList = new ArrayList<>();
        inlineList.add(new EmailInline("usefultools-logo", new ClassPathResource(LOGO_IMAGE), EmailInline.PNG_MIME));
        inlineList.add(new EmailInline("pass-reset", new ClassPathResource(PASS_RESET_IMAGE), EmailInline.JPG_MIME));

        try {
            return sendEditableMail(recipientEmail,
                    "Password Reset",
                    contextVariables,
                    inlineList,
                    2);
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean sendErrorEmail(String errorMessage) {
        try {
            return sendTextMail("Error in Useful Tools", "bgarver@outlook.com", errorMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Scheduled(cron = "0 59 23 * * *")
    public boolean sendDailyAccessSummary() {
        try {
            List<IpAccess> ipAccessList = ipAccessService.getDailySummary();

            StringBuilder stringBuilder = new StringBuilder();

            for (IpAccess ipAccess : ipAccessList) {
                String url = ipAccess.getFullUrl().replace("https://", "").replace("www.", "").replace(".com", "");
                stringBuilder.append("Ip: ").append(ipAccess.getIpAddress()).append(" | ")
                        .append("Path: ").append(url).append(" | ")
                        .append("Date/Time: ").append(DateFormat.getDateTimeInstance().format(ipAccess.getDateTime()))
                        .append("\n\n");
            }
            return sendTextMail("Daily Summary of UsefulTools", "bgarver@outlook.com", stringBuilder.toString());
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    private String resolveTemplate(String template) throws IOException {
        Resource templateResource = this.applicationContext.getResource(template);
        InputStream inputStream = templateResource.getInputStream();
        return IOUtils.toString(inputStream, EmailConfiguration.EMAIL_TEMPLATE_ENCODING);
    }
}
