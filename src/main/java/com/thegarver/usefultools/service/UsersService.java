package com.thegarver.usefultools.service;

import com.thegarver.usefultools.dao.Login;
import com.thegarver.usefultools.dao.UserSettings;
import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.dao.repository.UserSettingsRepository;
import com.thegarver.usefultools.dao.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UsersService {

    protected UsersRepository usersRepository;
    protected UserSettingsService userSettingsService;
    protected LoginService loginService;

    @Autowired
    public UsersService (UsersRepository usersRepository, UserSettingsService userSettingsService, LoginService loginService) {
        this.usersRepository = usersRepository;
        this.userSettingsService = userSettingsService;
        this.loginService = loginService;
    }

    public Users findUserByID(int id) {
        return usersRepository.findById(id);
    }

    public void createNewUser(Users users) {

        Users updatedUser = usersRepository.save(users);
        UserSettings userSettings = new UserSettings();

        //Default settings
        userSettings.setUsers(updatedUser);
        userSettings.setDarkMode(false);
        userSettings.setTcHours(TcTimeService.DEFAULT_HOURS);
        userSettings.setTcWage(TcTimeService.DEFAULT_WAGE);
        userSettings.setInfoSeen(false);
        userSettingsService.createNewUserSettings(userSettings);
    }

    public Users findUserFromEmail(String email) {
        Optional<Login> login = loginService.findLoginByEmail(email);

        return login.map(value -> usersRepository.findById(value.getUsers().getId())).orElse(null);
    }

    public void saveUser(Users users) {
        usersRepository.save(users);
    }

    public void removeProfile(String email) {
        usersRepository.delete(findUserFromEmail(email));
    }
}
