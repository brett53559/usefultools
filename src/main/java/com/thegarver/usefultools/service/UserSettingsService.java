package com.thegarver.usefultools.service;

import com.thegarver.usefultools.dao.UserSettings;
import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.dao.repository.UserSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserSettingsService {

    protected UserSettingsRepository usersSettingsRepository;

    @Autowired
    public UserSettingsService(UserSettingsRepository usersSettingsRepository) {
        this.usersSettingsRepository = usersSettingsRepository;
    }

    public void createNewUserSettings(UserSettings userSettings) {
        usersSettingsRepository.save(userSettings);
    }

    public UserSettings findUserSettings(int id) {
        return usersSettingsRepository.findById(id);
    }

    public void updateWageAndHours(Users users, double wage, int hours) {
        UserSettings userSettings = findUserSettings(users.getId());

        userSettings.setTcWage(wage);
        userSettings.setTcHours(hours);

        usersSettingsRepository.save(userSettings);
    }

    public void updateInfoSeen(Users users) {
        UserSettings userSettings = users.getUserSettings();

        userSettings.setInfoSeen(true);

        usersSettingsRepository.save(userSettings);
    }

    public void updateDarkMode(Users users, boolean darkModeEnabled) {
        UserSettings userSettings = users.getUserSettings();

        userSettings.setDarkMode(darkModeEnabled);

        usersSettingsRepository.save(userSettings);
    }

    public void updateSfStartCode(Users users, int code) {
        UserSettings userSettings = users.getUserSettings();

        userSettings.setSfStartCode(code);

        usersSettingsRepository.save(userSettings);
    }
}
