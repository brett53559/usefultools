package com.thegarver.usefultools.configuration;

import com.thegarver.usefultools.dao.IpAccess;
import com.thegarver.usefultools.dto.IpBlock;
import com.thegarver.usefultools.service.RequestService;
import com.thegarver.usefultools.service.IpAccessService;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.SmartView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.*;

@Component
public class MyHandlerInterceptor extends HandlerInterceptorAdapter {

    private static Logger log = LoggerFactory.getLogger(MyHandlerInterceptor.class);

    @Value("${pageLinks.timeCalc}")
    private String timeCalc;

    @Value("${pageLinks.shiftFinder}")
    private String shiftFinder;

    @Value("${pageLinks.shiftSelection}")
    private String shiftSelection;

    @Value("${pageLinks.deobfuscator}")
    private String deobfuscator;

    @Value("${pageLinks.profile}")
    private String profile;

    @Value("${pageLinks.login}")
    private String login;

    @Value("${domain.current}")
    private String domain;

    @Value("${disallowed.items.list}")
    private String disallowedWords;

    private IpAccessService ipAccessService;

    private RequestService requestService;

    private List<IpBlock> ipBlockList = new ArrayList<>();

    @Autowired
    public MyHandlerInterceptor(IpAccessService ipAccessService,
                                RequestService requestService) {
        this.ipAccessService = ipAccessService;
        this.requestService = requestService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

        if (request.getMethod().equalsIgnoreCase("POST") || (modelAndView != null && !isRedirectView(modelAndView))) {

            log.info(clientsUsername() + " has accessed " + requestService.getRealUrl(request) + " from " + requestService.getRemoteAddr(request) + " via " + request.getMethod() + " method");

            if (bothIpAreSame(request)) {
                String parameters = requestService.getParameters(request);

                //If a url contains anything other than thegarver it's blocked
                if (!request.getRequestURL().toString().contains(domain)) {
                    requestService.addIpToServerFirewall(request, "Didn't use thegarver");
                    return;
                }

                List<String> disallowedParams = new ArrayList<>();
                File file = new File(disallowedWords);
                Scanner scanner = new Scanner(file);
                while (scanner.hasNextLine()) {
                    disallowedParams.add(scanner.nextLine());
                }

                //if a url has a parameter included, check to see if it's a defined parameter and if it's not, it's blocked
                if (!parameters.equalsIgnoreCase("?")) {
                    boolean paramFound = false;
                    for (String param : disallowedParams) {
                        if (parameters.contains(param)) {
                            log.info("Matched invalid parameter " + param);
                            paramFound = true;
                        }
                    }
                    if (paramFound) {
                        requestService.addIpToServerFirewall(request, "invalid URL Parameter");
                        return;
                    }
                } else if (request.getRequestURL().toString().contains("error")) {
                    //If url is for the error controller, see if it's the 10th time within 24 hours and block it
                    IpBlock ipBlock = ipBlockList.stream()
                            .filter(item -> item.getIpAddress().equalsIgnoreCase(request.getRemoteAddr()))
                            .findFirst()
                            .orElse(new IpBlock(request.getRemoteAddr()));
                    if (ipBlock.getAccessCount() == 0) {
                        ipBlock.addToCount();
                        ipBlockList.add(ipBlock);
                    } else {
                        ipBlock.addToCount();
                    }

                    long currentTimeMils = Calendar.getInstance().getTimeInMillis();
                    long timeDifference = currentTimeMils - ipBlock.getLastAccessed().getTimeInMillis();
                    if (timeDifference >= 86400000) {
                        ipBlockList.remove(ipBlock);
                    } else if(ipBlock.getAccessCount() >= 10) {
                        requestService.addIpToServerFirewall(request, "more than 10 errors in one day");
                        ipBlockList.remove(ipBlock);
                        return;
                    } else {
                        ipBlock.updateLastAccess();
                    }
                }

                //See if the IP has access this site before, if so, see if it's making too many requests per second.
                if (ipAccessService.doesIpAddressExist(request.getRemoteAddr())) {
                    if (ipAccessService.withinSecondCount(request.getRemoteAddr()) >= 4) {
                        requestService.addIpToServerFirewall(request, "a potential DDos");
                        return;
                    }
                }
                createNewIpError(request, requestService.getRealUrl(request));
            } else {
                requestService.addIpToServerFirewall(request, "IP's not matching");
                return;
            }

            boolean darkModeEnabled = false;
            boolean homepage = false;

            if (!request.getMethod().equalsIgnoreCase("POST")) {
                if (isUserLogged()) {
                    addToModelUserDetails(modelAndView);
                    modelAndView.addObject("userLoggedIn", true);
                    //todo use this to show user name on navbar
                }

                Cookie[] cookies = request.getCookies();
                if (cookies != null) {
                    for (Cookie cookie : cookies) {
                        if (cookie.getName().equals(GlobalStaticVars.COOKIE_DARKMODE_NAME) && cookie.getValue().equalsIgnoreCase("true")) {
                            darkModeEnabled = true;
                        }
                    }
                }

                if (request.getServletPath().equalsIgnoreCase("/")) {
                    homepage = true;
                }

                //Page Links
                modelAndView.addObject("homepage", homepage);
                modelAndView.addObject("darkModeEnabled", darkModeEnabled);
                modelAndView.addObject("loginLink", "/" + login);
                modelAndView.addObject("timeCalcLink", "/" + timeCalc);
                modelAndView.addObject("shiftFinderLink", "/" + shiftFinder);
                modelAndView.addObject("shiftSelectionLink", "/" + shiftSelection);
                modelAndView.addObject("deobfuscatorLink", "/" + deobfuscator);
                modelAndView.addObject("profileLink", "/" + profile);

                //Cookie Names
                modelAndView.addObject("cookieSfStartCode", GlobalStaticVars.COOKIE_SF_START_CODE);
            }
        }
    }

    public static boolean isRedirectView(ModelAndView mv) {
        String viewName = mv.getViewName();
        if (viewName.startsWith(GlobalStaticVars.RETURN_REDIRECT)) {
            return true;
        }
        View view = mv.getView();
        return (view != null && view instanceof SmartView
                && ((SmartView) view).isRedirectView());
    }

    private String clientsUsername() {
        try {
            return SecurityContextHolder.getContext().getAuthentication().getName();
        } catch (Exception e) {
            return "Error";
        }
    }

    public boolean isUserLogged() {
        return !clientsUsername().equals("anonymousUser");
    }

    private void addToModelUserDetails(ModelAndView model) {
        String loggedEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addObject("loggedEmail", loggedEmail);
    }

    private boolean bothIpAreSame(HttpServletRequest request) {
        String mainIp = request.getRemoteAddr();
        String headerIp = request.getHeader("X-FORWARDED-FOR");

        if (headerIp != null) {
            return headerIp.equalsIgnoreCase(mainIp);
        }
        return true;
    }

    private void createNewIpError(HttpServletRequest request, String url) {
        IpAccess ipAccess = new IpAccess();

        ipAccess.setIpAddress(request.getRemoteAddr());

        ipAccess.setFullUrl(request.getMethod() + " | " + url);
        if (request.getRequestURL().toString().contains("error")) ipAccess.setError(true);
        ipAccess.setDateTime(new Date());
        
        ipAccessService.addNewIpAccess(ipAccess);
    }
}