package com.thegarver.usefultools.configuration;

import com.thegarver.usefultools.customErrors.CustomAuthenticationFailureHandler;
import com.thegarver.usefultools.service.MyUserDetailsService;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final String PROFILE_PAGE;
    private final String TIMECALC_PAGE;
    private final String REGISTER_PAGE;
    private final String PASS_RESET_PAGE;
    private final String FORGOT_PASS_PAGE;
    private final String LOGIN_PAGE;
    private final String LOGOUT_PAGE;
    private final String SHIFT_FINDER_PAGE;
    private final String SHIFT_SELECTION_PAGE;
    private final String DEOBFUSCATOR_PAGE;

    MyUserDetailsService myUserDetailsService;

    @Autowired
    public SecurityConfiguration(MyUserDetailsService myUserDetailsService,
                                 @Value("${pageLinks.profile}") String profilePage,
                                 @Value("${pageLinks.timeCalc}") String timeCalcPage,
                                 @Value("${pageLinks.register}") String registerPage,
                                 @Value("${pageLinks.passreset}") String passResetPage,
                                 @Value("${pageLinks.forgotpass}") String forgotPassPage,
                                 @Value("${pageLinks.login}") String loginPage,
                                 @Value("${pageLinks.logout}") String logoutPage,
                                 @Value("${pageLinks.shiftFinder}") String shiftFinderPage,
                                 @Value("${pageLinks.shiftSelection}") String shiftSelectionPage,
                                 @Value("${pageLinks.deobfuscator}") String deobfuscatorPage) {
        this.myUserDetailsService = myUserDetailsService;
        this.PROFILE_PAGE = profilePage;
        this.TIMECALC_PAGE = timeCalcPage;
        this.REGISTER_PAGE = registerPage;
        this.PASS_RESET_PAGE = passResetPage;
        this.FORGOT_PASS_PAGE = forgotPassPage;
        this.LOGIN_PAGE = loginPage;
        this.LOGOUT_PAGE = logoutPage;
        this.SHIFT_FINDER_PAGE = shiftFinderPage;
        this.SHIFT_SELECTION_PAGE = shiftSelectionPage;
        this.DEOBFUSCATOR_PAGE = deobfuscatorPage;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        String logout = "/" + LOGOUT_PAGE;

        http.authorizeRequests()
                .antMatchers("/" + PROFILE_PAGE,
                        "/" + PROFILE_PAGE + "/**",
                        "/" + TIMECALC_PAGE,
                        "/" + TIMECALC_PAGE + "/**").hasAnyAuthority("ROLES_USER", "ROLES_ADMIN")
                .antMatchers(logout).authenticated()
                .antMatchers("static/**", "templates/**",
                        "/" + REGISTER_PAGE,
                        "/" + PASS_RESET_PAGE,
                        "/" + FORGOT_PASS_PAGE,
                        "/" + LOGIN_PAGE,
                        "/" + DEOBFUSCATOR_PAGE,
                        "/" + SHIFT_FINDER_PAGE,
                        "/" + SHIFT_SELECTION_PAGE,
                        "/",
                        "/error").permitAll()
                .and()
                .formLogin()
                .loginPage("/" + LOGIN_PAGE).usernameParameter("email").passwordParameter("password").defaultSuccessUrl("/").permitAll().failureHandler(authenticationFailureHandler())
                .and().rememberMe().key("uniqueAndSecret").tokenValiditySeconds(172800);

        http.logout().logoutUrl(logout).logoutSuccessUrl("/?" + GlobalStaticVars.URL_PARAM_LOGOUT).invalidateHttpSession(true).deleteCookies("JSESSIONID");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }
}
