package com.thegarver.usefultools.dao;

import com.thegarver.usefultools.userInput.ClockInOut;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "TC_TIMES")
@Access(value = AccessType.FIELD)
public class TcTimes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    //@ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL )
    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private Users users;

    @Column(name = "DAY")
    private int day;

    @Column(name = "SHIFT_COUNT")
    private int shiftCount;

    @Temporal(TemporalType.TIME)
    @Column(name = "TIME_IN")
    private Date timeIn;

    @Temporal(TemporalType.TIME)
    @Column(name = "TIME_OUT")
    private Date timeOut;

    public TcTimes() {
    }

    public TcTimes(Users users, int day, int shiftCount, Date timeIn, Date timeOut) {
        this.users = users;
        this.day = day;
        this.shiftCount = shiftCount;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getShiftCount() {
        return shiftCount;
    }

    public void setShiftCount(int shiftCount) {
        this.shiftCount = shiftCount;
    }

    public Date getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(Date timeIn) {
        this.timeIn = timeIn;
    }

    public Date getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
    }

    public long getMillisecondsWorked() {
        if (timeOut != null) {
            return timeOut.getTime() - timeIn.getTime();
        }
        return 0;
    }

    public ClockInOut convertToUserInput() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String parsedTimeOut = "";
        if (timeOut != null) {
            parsedTimeOut = timeFormat.format(timeOut);
        }
        return new ClockInOut(day, shiftCount, timeFormat.format(timeIn), parsedTimeOut);
    }
}
