package com.thegarver.usefultools.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "LOGIN")
@Access(value= AccessType.FIELD)
public class Login implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int")
    private int id;

    @Column(name = "EMAIL", columnDefinition = "varchar(max)")
    private String email;

    @Column(name = "PASS_HASH", columnDefinition = "varchar(max)")
    private String passHash;

    @Column(name = "ROLES", columnDefinition = "varchar(max)")
    private String roles;

    @Column(name = "ACTIVE", columnDefinition = "bit")
    private boolean active;

    @Column(name = "ACTIVATION_ID", columnDefinition = "varchar(max)")
    private String activationId;

    @Column(name = "ACTIVATION_EXPIRATION", columnDefinition = "datetime")
    private Date activationExpiration;

    @Column(name = "RESET_ID", columnDefinition = "varchar(max)")
    private String resetId;

    @Column(name = "RESET_EXPIRATION", columnDefinition = "datetime")
    private Date resetExpiration;

    //@OneToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL )
    @OneToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private Users users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getActivationId() {
        return activationId;
    }

    public void setActivationId(String activationId) {
        this.activationId = activationId;
    }

    public Date getActivationExpiration() {
        return activationExpiration;
    }

    public void setActivationExpiration(Date activationExpiration) {
        this.activationExpiration = activationExpiration;
    }

    public String getResetId() {
        return resetId;
    }

    public void setResetId(String resetId) {
        this.resetId = resetId;
    }

    public Date getResetExpiration() {
        return resetExpiration;
    }

    public void setResetExpiration(Date resetExpiration) {
        this.resetExpiration = resetExpiration;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Login{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", passHash='" + passHash + '\'' +
                ", roles='" + roles + '\'' +
                ", active=" + active +
                ", activationId='" + activationId + '\'' +
                ", activationExpiration=" + activationExpiration +
                ", resetId='" + resetId + '\'' +
                ", resetExpiration=" + resetExpiration +
                ", users=" + users +
                '}';
    }
}
