package com.thegarver.usefultools.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "USER_SETTINGS")
@Access(value= AccessType.FIELD)
public class UserSettings implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int")
    private int id;

    @Column(name = "DARK_MODE", columnDefinition = "bit")
    private boolean darkMode;

    @Column(name = "TC_WAGE", columnDefinition = "money")
    private double tcWage;

    @Column(name = "TC_HOURS", columnDefinition = "int")
    private int tcHours;

    @Column(name = "TC_INFOSEEN", columnDefinition = "bit")
    private boolean infoSeen;

    @Column(name = "SF_STARTCODE", columnDefinition = "int")
    private int sfStartCode;

    //@OneToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL )
    @OneToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private Users users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isDarkMode() {
        return darkMode;
    }

    public void setDarkMode(boolean darkMode) {
        this.darkMode = darkMode;
    }

    public double getTcWage() {
        return tcWage;
    }

    public void setTcWage(double tcWage) {
        this.tcWage = tcWage;
    }

    public int getTcHours() {
        return tcHours;
    }

    public void setTcHours(int tcHours) {
        this.tcHours = tcHours;
    }

    public boolean isInfoSeen() {
        return infoSeen;
    }

    public void setInfoSeen(boolean infoSeen) {
        this.infoSeen = infoSeen;
    }

    public int getSfStartCode() {
        return sfStartCode;
    }

    public void setSfStartCode(int sfStartCode) {
        this.sfStartCode = sfStartCode;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "UserSettings{" +
                "id=" + id +
                ", darkMode=" + darkMode +
                ", tcWage=" + tcWage +
                ", tcHours=" + tcHours +
                ", infoSeen=" + infoSeen +
                ", sfStartCode=" + sfStartCode +
                ", users=" + users +
                '}';
    }
}
