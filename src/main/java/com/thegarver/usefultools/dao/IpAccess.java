package com.thegarver.usefultools.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IP_ACCESS")
@Access(value= AccessType.FIELD)
public class IpAccess implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "IP_ADDRESS", columnDefinition = "varchar(20)")
    private String ipAddress;

    @Column(name = "FULL_URL", columnDefinition = "varchar(MAX  )")
    private String fullUrl;

    @Column(name = "ERROR")
    private boolean error;

    @Column(name = "DATETIME")
    private Date dateTime;

    public IpAccess() {
    }

    public IpAccess(int id, String ipAddress, String fullUrl, boolean error, Date dateTime) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.fullUrl = fullUrl;
        this.error = error;
        this.dateTime = dateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
