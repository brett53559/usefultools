package com.thegarver.usefultools.dao.repository;

import com.thegarver.usefultools.dao.Login;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface LoginRepository extends CrudRepository<Login, String> {

    @Query
    Optional<Login> findByEmail(String email);

    @Query
    Optional<Login> findByActivationIdLikeAndActivationExpirationAfter(String id, Date date);

    @Query
    Optional<Login> findByResetIdLikeAndResetExpirationAfter(String id, Date date);
}
