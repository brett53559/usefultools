package com.thegarver.usefultools.dao.repository;

import com.thegarver.usefultools.dao.IpAccess;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface IpAccessRepository extends CrudRepository<IpAccess, String> {

    @Query
    List<IpAccess> findByIpAddressLikeOrderByIdDesc(String ipAddress);

    @Query
    Optional<IpAccess> findDistinctFirstByIpAddress(String ipAddress);

    @Query
    List<IpAccess> findAllByIdNotNull();

    @Query
    List<IpAccess> findAllByDateTimeAfter(Date date);
}
