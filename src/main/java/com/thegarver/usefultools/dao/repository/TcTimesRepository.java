package com.thegarver.usefultools.dao.repository;

import com.thegarver.usefultools.dao.TcTimes;
import com.thegarver.usefultools.dao.Users;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TcTimesRepository extends CrudRepository<TcTimes, String> {

    @Query
    List<TcTimes> findAllByUsersOrderByDayDescShiftCountDesc(Users users);

    @Query
    @Modifying
    void deleteAllById(int id);
}
