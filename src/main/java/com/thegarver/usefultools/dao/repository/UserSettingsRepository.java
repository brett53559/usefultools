package com.thegarver.usefultools.dao.repository;

import com.thegarver.usefultools.dao.UserSettings;
import com.thegarver.usefultools.dao.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSettingsRepository extends CrudRepository<UserSettings, String> {

    @Query
    UserSettings findById(int id);
}
