package com.thegarver.usefultools.jsonObj;

public class ShiftFindCalEvents {

    private String title = "On the grind.";
    private String start;
    private String end;
    private String url = "https://whentowork.com";

    public ShiftFindCalEvents() {
    }

    public ShiftFindCalEvents(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public ShiftFindCalEvents(String start, String end, String title) {
        this.title = title;
        this.start = start;
        this.end = end;
        this.url = "";
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTitle() {
        return title;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getUrl() {
        return url;
    }

    public static class ShiftFindCalEventWithBgColor extends ShiftFindCalEvents{
        private final String backGroundColor;

        public ShiftFindCalEventWithBgColor(String start, String end, String backGroundColor) {
            super(start, end);
            this.backGroundColor = backGroundColor;
        }

        public ShiftFindCalEventWithBgColor(String start, String end, String title, String backGroundColor) {
            super(start, end, title);
            this.backGroundColor = backGroundColor;
        }

        public String getBackgroundColor() {
            return backGroundColor;
        }
    }
}
