package com.thegarver.usefultools.customErrors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationFailureHandler
  implements AuthenticationFailureHandler {

    @Value("${pageLinks.login}")
    private String LOGIN_PAGE;
 
    private ObjectMapper objectMapper = new ObjectMapper();
 
    @Override
    public void onAuthenticationFailure(
      HttpServletRequest request,
      HttpServletResponse response,
      AuthenticationException exception)
      throws IOException, ServletException {
 
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        if (exception.getMessage().toLowerCase().contains("bad")) {
            response.sendRedirect("/" + LOGIN_PAGE + "?" + GlobalStaticVars.URL_PARAM_BADLOGIN);
        } else if (exception.getMessage().toLowerCase().contains("disabled")) {
            response.sendRedirect("/" + LOGIN_PAGE + "?" + GlobalStaticVars.URL_PARAM_USERDISABLED);
        } else response.sendRedirect("/" + LOGIN_PAGE + "?" + GlobalStaticVars.URL_PARAM_AUTHENTICATIONERROR);
    }
}