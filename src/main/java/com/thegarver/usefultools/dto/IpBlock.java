package com.thegarver.usefultools.dto;

import java.util.Calendar;
import java.util.Date;

public class IpBlock {
    private String ipAddress;
    private Calendar lastAccessed = Calendar.getInstance();
    private int accessCount = 0;

    public IpBlock(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Calendar getLastAccessed() {
        return lastAccessed;
    }

    public int getAccessCount() {
        return accessCount;
    }

    public void updateLastAccess() {
        lastAccessed.setTime(new Date());
    }

    public void addToCount() {
        accessCount++;
    }
}
