package com.thegarver.usefultools.dto;

import org.springframework.core.io.InputStreamSource;

public class EmailInline {
    public static final String PNG_MIME = "image/png";
    public static final String JPG_MIME = "image/jpg";

    private String contentId;
    private InputStreamSource inputStreamSource;
    private String contentType;

    public EmailInline(String contentId, InputStreamSource inputStreamSource, String contentType) {
        this.contentId = contentId;
        this.inputStreamSource = inputStreamSource;
        this.contentType = contentType;
    }

    public String getContentId() {
        return contentId;
    }

    public InputStreamSource getInputStreamSource() {
        return inputStreamSource;
    }

    public String getContentType() {
        return contentType;
    }
}
