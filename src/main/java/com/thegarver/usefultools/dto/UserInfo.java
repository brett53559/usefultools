package com.thegarver.usefultools.dto;

public class UserInfo {
    private String firstName = "";
    private String lastName = "";
    private String currentEmail = "";
    private String newEmail = "";
    private String password = "";
    private boolean passwordValid = true;

    public UserInfo() {
    }

    public UserInfo(String firstName, String lastName, String currentEmail, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.currentEmail = currentEmail;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCurrentEmail() {
        return currentEmail;
    }

    public void setCurrentEmail(String currentEmail) {
        this.currentEmail = currentEmail;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isPasswordValid() {
        return passwordValid;
    }

    public void setPasswordValid(boolean passwordValid) {
        this.passwordValid = passwordValid;
    }
}
