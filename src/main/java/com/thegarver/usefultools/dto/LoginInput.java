package com.thegarver.usefultools.dto;

public class LoginInput {
    private String email;
    private String password;
    private boolean rememberMe;
    private boolean comboValid = true;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public boolean isComboValid() {
        return comboValid;
    }

    public void setComboValid(boolean comboValid) {
        this.comboValid = comboValid;
    }
}
