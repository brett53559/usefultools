package com.thegarver.usefultools.controllers;

import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.service.UserSettingsService;
import com.thegarver.usefultools.service.UsersService;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class ShiftSelectionController {

    private final String CURRENT_PAGE;

    @Autowired
    public ShiftSelectionController(@Value("${pageLinks.shiftSelection}") String currentPage) {
        this.CURRENT_PAGE = currentPage;
    }

    @GetMapping("/${pageLinks.shiftSelection}")
    public String openShiftSelection(Model model) {
        SimpleDateFormat fullDateTime = new SimpleDateFormat("EEEE MMMM dd yyyy - h:mm aa");
        model.addAttribute("timeDate", fullDateTime.format(new Date()));

        return CURRENT_PAGE;
    }
}
