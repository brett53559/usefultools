package com.thegarver.usefultools.controllers;

import com.amazonaws.util.ImmutableMapParameter;
import com.thegarver.usefultools.util.MultiPartFileToFile;
import com.thegarver.usefultools.util.S3FileIntractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Map;

@Controller
public class FileController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    /*
    A page request has been submitted with a file as an argument
    Instantiate the S3 File Interactor class
    Conver the multipart file into a standard file and then pass over to the interactor class for upload.
     */
    @PostMapping("/file")
    @ResponseBody
    public Map<String,String> imageUpload(HttpServletRequest request, @RequestParam("mediafile") MultipartFile file) {
        MultiPartFileToFile multiPartFileToFile = new MultiPartFileToFile(file);
        S3FileIntractor s3FileIntractor = new S3FileIntractor();

        File convertedFile = null;
        try {
            convertedFile = multiPartFileToFile.getConvertedFile();
            String AWSFilePath = s3FileIntractor.UploadFile(convertedFile);
            convertedFile.delete();
            return ImmutableMapParameter.of("link", AWSFilePath);
        } catch (Exception e) {
            if (convertedFile != null) convertedFile.delete();
            LOGGER.error(e.getMessage());
            return ImmutableMapParameter.of("error", e.getMessage());
        }
    }
}