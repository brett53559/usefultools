package com.thegarver.usefultools.controllers.securityAndAuth;

import com.thegarver.usefultools.dao.Login;
import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.dto.ForgotPassword;
import com.thegarver.usefultools.service.EmailService;
import com.thegarver.usefultools.service.LoginService;
import com.thegarver.usefultools.service.UsersService;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.UUID;

@Controller
public class ForgotPasswordController {
    private final String CURRENT_PAGE;

    LoginService loginService;
    UsersService usersService;
    EmailService emailService;

    @Autowired
    public ForgotPasswordController(LoginService loginService, UsersService usersService, EmailService emailService, @Value("/${pageLinks.forgotpass}") String currentPage) {
        this.loginService = loginService;
        this.usersService = usersService;
        this.emailService = emailService;
        this.CURRENT_PAGE = currentPage;
    }

    @GetMapping("/${pageLinks.forgotpass}")
    public String openFogotPassword(Model model){
        model.addAttribute("forgotPasswordInput", new ForgotPassword());
        return CURRENT_PAGE;
    }

    @PostMapping("/${pageLinks.forgotpass}")
    public String submitForgotPassword(HttpServletResponse response, @ModelAttribute ForgotPassword forgotPassword, Model model) {
        forgotPassword.setEmail(forgotPassword.getEmail().trim());
        Optional<Login> temp = loginService.findLoginByEmail(forgotPassword.getEmail());

        if (temp.isPresent()) {
            String resetId = UUID.randomUUID().toString();
            Users users = usersService.findUserFromEmail(forgotPassword.getEmail());
            Login login = temp.get();

            login.setResetId(resetId);
            login.setResetExpiration(DateTime.now().plusHours(2).toDate());
            loginService.saveLogin(login);

            emailService.sendPassResetEmail(forgotPassword.getEmail(), users.getFirstName() + " " + users.getLastName(), resetId);
        }
        return GlobalStaticVars.RETURN_REDIRECT + "?" + GlobalStaticVars.URL_PARAM_FORGOTPASS;
    }
}
