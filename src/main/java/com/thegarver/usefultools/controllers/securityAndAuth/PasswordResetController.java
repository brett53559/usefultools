package com.thegarver.usefultools.controllers.securityAndAuth;

import com.thegarver.usefultools.dao.Login;
import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.dto.PasswordReset;
import com.thegarver.usefultools.dto.UserInfo;
import com.thegarver.usefultools.service.LoginService;
import com.thegarver.usefultools.service.UsersService;
import com.thegarver.usefultools.util.GlobalStaticVars;
import com.thegarver.usefultools.util.PasswordValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
public class PasswordResetController {
    private final String CURRENT_PAGE;
    private final String LOGIN_PAGE;

    LoginService loginService;
    UsersService usersService;

    @Autowired
    public PasswordResetController(LoginService loginService, UsersService usersService,
                                   @Value("${pageLinks.passreset}") String currentPage,
                                   @Value("${pageLinks.login}") String loginPage) {
        this.loginService = loginService;
        this.usersService = usersService;
        this.CURRENT_PAGE = currentPage;
        this.LOGIN_PAGE = loginPage;
    }

    @GetMapping("/${pageLinks.passreset}")
    public String openPassReset(Model model, @RequestParam(name = GlobalStaticVars.URL_PARAM_PASSRESETID)String id){
        Optional<Login> temp = loginService.findLoginFromResetId(id);

        if (!temp.isPresent()) {
            return GlobalStaticVars.RETURN_REDIRECT + LOGIN_PAGE + "?" + GlobalStaticVars.URL_PARAM_AUTHENTICATIONERROR;
        }

        PasswordReset passwordReset = new PasswordReset();
        passwordReset.setEmail(temp.get().getEmail());
        model.addAttribute("passwordResetLink", "/" + CURRENT_PAGE);
        model.addAttribute("passwordResetInput", passwordReset);
        return CURRENT_PAGE;
    }

    @PostMapping("/${pageLinks.passreset}")
    public String submitPasswordReset(HttpServletResponse response, @ModelAttribute PasswordReset passwordResetInput, Model model) {
        Users users = usersService.findUserFromEmail(passwordResetInput.getEmail());
        Login login = loginService.findLoginByEmail(passwordResetInput.getEmail()).get();

        PasswordValidationUtil passwordValidationUtil = new PasswordValidationUtil();

        UserInfo userInfo = new UserInfo(users.getFirstName(), users.getLastName(), passwordResetInput.getEmail(), passwordResetInput.getPassword1());

        //Password was validated and current password is not the submitted password.
        if (passwordValidationUtil.validatePassword(model, userInfo) &&
                !PasswordEncoderFactories.createDelegatingPasswordEncoder().matches(passwordResetInput.getPassword1().trim(), login.getPassHash())) {

            login.setPassHash(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(passwordResetInput.getPassword1().trim()));

            login.setResetId(null);
            login.setResetExpiration(null);
            loginService.saveLogin(login);
        } else {
            passwordResetInput.setPasswordValid(false);
            model.addAttribute("passwordResetInput", passwordResetInput);
            return CURRENT_PAGE;
        }
        return GlobalStaticVars.RETURN_REDIRECT + LOGIN_PAGE + "?" + GlobalStaticVars.URL_PARAM_PASSRESET;
    }
}
