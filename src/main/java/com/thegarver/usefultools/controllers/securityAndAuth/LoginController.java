package com.thegarver.usefultools.controllers.securityAndAuth;

import com.thegarver.usefultools.service.UserSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
    private final String CURRENT_PAGE;

    @Autowired
    public LoginController(@Value("${pageLinks.login}") String currentPage) {
        this.CURRENT_PAGE = currentPage;
    }

    @GetMapping("/${pageLinks.login}")
    public String openLogin(Model model,
                            @RequestParam(name = "useractivated", defaultValue = "") String userActivated,
                            @RequestParam(name = "newuser", defaultValue = "") String newUser) {
        String email = "";
        if (!userActivated.isEmpty()) email = userActivated;
        if (!newUser.isEmpty()) email = newUser;

        model.addAttribute("passedInEmail", email);
        return CURRENT_PAGE;
    }
}
