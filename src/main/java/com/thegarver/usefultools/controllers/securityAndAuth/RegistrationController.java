package com.thegarver.usefultools.controllers.securityAndAuth;

import com.thegarver.usefultools.dao.Login;
import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.dto.UserInfo;
import com.thegarver.usefultools.service.EmailService;
import com.thegarver.usefultools.service.LoginService;
import com.thegarver.usefultools.service.UsersService;
import com.thegarver.usefultools.util.GlobalStaticVars;
import com.thegarver.usefultools.util.PasswordValidationUtil;
import com.thegarver.usefultools.util.TrimObjectStrings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.UUID;

@Controller
public class RegistrationController {
    private final String CURRENT_PAGE;
    private final String LOGIN_PAGE;

    LoginService loginService;
    UsersService usersService;
    EmailService emailService;

    public RegistrationController(LoginService loginService, UsersService usersService, EmailService emailService,
                                  @Value("${pageLinks.register}") String currentPage,
                                  @Value("${pageLinks.login}") String loginPage) {
        this.loginService = loginService;
        this.usersService = usersService;
        this.emailService = emailService;
        this.CURRENT_PAGE = currentPage;
        this.LOGIN_PAGE = loginPage;
    }

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @GetMapping("/${pageLinks.register}")
    public String openRegistration(WebRequest webRequest, Model model){
        UserInfo userInfo = new UserInfo();

        model.addAttribute("newUser", userInfo);
        return CURRENT_PAGE;
    }

    @GetMapping("/${pageLinks.register}/activate") /*Make sure to change this in the welcome email too*/
    public String activateUser(Model model, @RequestParam(GlobalStaticVars.URL_PARAM_ACTIVATIONID) String id) {

        Optional<Login> temp = loginService.findLoginFromActivationId(id);

        if (temp.isPresent()) {
            Login login = temp.get();

            login.setActive(true);
            login.setActivationExpiration(null);
            login.setActivationId(null);
            loginService.saveLogin(login);

            return GlobalStaticVars.RETURN_REDIRECT + LOGIN_PAGE + "?" + GlobalStaticVars.URL_PARAM_USERACTIVATED + "=" + login.getEmail();
        }
        return CURRENT_PAGE + "?" + GlobalStaticVars.URL_PARAM_ACTIVATIONIDINVALID;
    }

    @PostMapping("/${pageLinks.register}")
    public String processRegistration(HttpServletResponse response, @ModelAttribute UserInfo userInfo, Model model) {
        model.addAttribute("newUser", userInfo);

        try {
            userInfo = (UserInfo) TrimObjectStrings.trimReflective(userInfo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        Users users = new Users();

        String tempFirst = Character.toUpperCase(userInfo.getFirstName().charAt(0)) + userInfo.getFirstName().substring(1);
        String tempLast = Character.toUpperCase(userInfo.getLastName().charAt(0)) + userInfo.getLastName().substring(1);
        users.setFirstName(tempFirst);
        users.setLastName(tempLast);

        userInfo.setFirstName(tempFirst);
        userInfo.setLastName(tempLast);

        //Now create the login info
        Login login = new Login();

        login.setUsers(users);
        login.setActive(false);
        login.setRoles("ROLES_USER");

        //Verify that the email isn't already registered
        if (loginService.doesEmailExist(userInfo.getCurrentEmail())){
            return GlobalStaticVars.RETURN_REDIRECT + CURRENT_PAGE + "?"+ GlobalStaticVars.URL_PARAM_EMAILTAKEN;
        } else {
            login.setEmail(userInfo.getCurrentEmail());
        }

        if (new PasswordValidationUtil().validatePassword(model, userInfo)) {
            login.setPassHash(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(userInfo.getPassword()));
        } else {
            userInfo.setPasswordValid(false);
            return CURRENT_PAGE;
        }

        //If the password and email are good then continue the registration process.
        String activationId = UUID.randomUUID().toString();

        login.setActivationId(activationId);
        login.setActivationExpiration(DateTime.now().plusDays(2).toDate());

        emailService.sendWelcomeEmail(userInfo.getCurrentEmail(), users.getFirstName(), activationId);

        usersService.createNewUser(users);
        loginService.createNewLogin(login);

        return GlobalStaticVars.RETURN_REDIRECT + LOGIN_PAGE + "?" + GlobalStaticVars.URL_PARAM_NEWUSER + "=" + userInfo.getCurrentEmail();
    }
}
