package com.thegarver.usefultools.controllers;

import com.thegarver.usefultools.service.RequestService;
import com.thegarver.usefultools.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class MyErrorController implements ErrorController {

    EmailService emailService;
    RequestService requestService;

    @Autowired
    public MyErrorController(EmailService emailService,
                             RequestService requestService) {
        this.emailService = emailService;
        this.requestService = requestService;
    }

    /*
    Get all Tomcat page errors and direct them here
    If it's a 404 error then display the 404.html page
    if it's anything other than that, display the error 500 page.
     */
    @GetMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());

            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                return "404";
            }
        }

        emailService.sendErrorEmail(request.toString());
        return "error";
    }

    @PostMapping("/error")
    @ResponseBody
    public String handlePostError(HttpServletRequest request) {
        emailService.sendErrorEmail("POST request made to " + requestService.getRealUrl(request) + " from " + requestService.getRealUrl(request));
        return "A post request on the error handler???? Wonder how that happened? ┬┴┬┴┤(･_├┬┴┬┴";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}