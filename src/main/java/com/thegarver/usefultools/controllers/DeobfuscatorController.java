package com.thegarver.usefultools.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DeobfuscatorController {

    String CURRENT_PAGE;

    @Autowired
    public DeobfuscatorController(@Value("${pageLinks.deobfuscator}") String page) {
        CURRENT_PAGE = page;
    }

    @GetMapping("/${pageLinks.deobfuscator}")
    public String displayHomePage(Model model) {
        return CURRENT_PAGE;
    }
}
