package com.thegarver.usefultools.controllers;

import com.thegarver.usefultools.dto.ChangePassword;
import com.thegarver.usefultools.dto.UserInfo;
import com.thegarver.usefultools.service.LoginService;
import com.thegarver.usefultools.service.ProfileService;
import com.thegarver.usefultools.util.GlobalStaticVars;
import com.thegarver.usefultools.util.PasswordValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@Controller
public class ProfileController {

    private final String CURRENT_PAGE;
    private final String LOGOUT_PAGE;

    ProfileService profileService;
    LoginService loginService;

    @Autowired
    public ProfileController(ProfileService profileService,
                             LoginService loginService,
                             @Value("${pageLinks.profile}") String currentPage,
                             @Value("${pageLinks.logout}") String logoutPage) {
        this.profileService = profileService;
        this.loginService = loginService;
        this.CURRENT_PAGE = currentPage;
        this.LOGOUT_PAGE = logoutPage;
    }

    @GetMapping("/${pageLinks.profile}")
    public String openProfile(Model model, Principal principal,HttpServletResponse response){
        UserInfo userInfo = profileService.getUserInfo(principal.getName());

        model.addAttribute("userInformation", userInfo);
        model.addAttribute("changePasswordInput", new ChangePassword());

        return CURRENT_PAGE;
    }

    @PostMapping("/${pageLinks.profile}/checkemail")
    @ResponseBody
    public String checkEmail(HttpServletResponse response, @RequestBody String data) {

        return loginService.doesEmailExist(data) ? "Email is currently taken." : "";
    }

    @PostMapping("/${pageLinks.profile}/updateemail")
    public String updateEmail(HttpServletResponse response, @ModelAttribute UserInfo userInfo, Principal principal) {
        if (userInfo.getNewEmail().isEmpty()) return CURRENT_PAGE;

        profileService.updateEmail(principal.getName(), userInfo.getNewEmail());
        return GlobalStaticVars.RETURN_REDIRECT + LOGOUT_PAGE;
    }

    @PostMapping("/${pageLinks.profile}/updatename")
    public String updateName(HttpServletResponse response, @ModelAttribute UserInfo userInfo, Principal principal) {

        profileService.updateName(userInfo.getFirstName(), userInfo.getLastName(), principal.getName());
        return GlobalStaticVars.RETURN_REDIRECT + CURRENT_PAGE + "?" + GlobalStaticVars.URL_PARAM_NAMEUPDATED;
    }

    @PostMapping("/${pageLinks.profile}/updatedarkmode")
    @ResponseBody
    public String updateDarkMode(HttpServletResponse response,
                                 @CookieValue(value = GlobalStaticVars.COOKIE_DARKMODE_NAME, defaultValue = "false") String darkModeCookie,
                                 @RequestBody String data, Principal principal) {
        Cookie cookie = new Cookie(GlobalStaticVars.COOKIE_DARKMODE_NAME, data);
        cookie.setPath("/");
        cookie.setMaxAge(60*60*24*52); //one year
        response.addCookie(cookie);

        boolean darkModeEnabled = false;

        if (data.equalsIgnoreCase("true")) {
            darkModeEnabled = true;
        }

        profileService.updateDarkMode(principal.getName(), darkModeEnabled);
        return "";
    }

    @PostMapping("/${pageLinks.profile}/changepassword")
    public String changePassword(HttpServletResponse response, @ModelAttribute ChangePassword changePassword, Model model, Principal principal) {

        if (!profileService.passwordIsValid(changePassword.getOldPassword(), principal.getName()))
        {
            return GlobalStaticVars.RETURN_REDIRECT + CURRENT_PAGE + "?" + GlobalStaticVars.URL_PARAM_OLDPASSINCORRECT;
        }

        UserInfo userInfo = profileService.getUserInfo(principal.getName());
        userInfo.setPassword(changePassword.getNewPassword1());
        PasswordValidationUtil passwordValidationUtil = new PasswordValidationUtil();
        if (passwordValidationUtil.validatePassword(model, userInfo)) {
            profileService.updatePassword(userInfo.getPassword(), principal.getName());
            return GlobalStaticVars.RETURN_REDIRECT + CURRENT_PAGE + "?" + GlobalStaticVars.URL_PARAM_PASSUPDATED;
        }
        return GlobalStaticVars.RETURN_REDIRECT + CURRENT_PAGE + "?" + GlobalStaticVars.URL_PARAM_INVALIDPASS;
    }

    @GetMapping("/${pageLinks.profile}/deleteprofile")
    public String deleteProfile(HttpServletResponse response, Principal principal) {
        profileService.removeProfile(principal.getName());
        return GlobalStaticVars.RETURN_REDIRECT + LOGOUT_PAGE;
    }
}
