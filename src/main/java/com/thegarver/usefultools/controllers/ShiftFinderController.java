package com.thegarver.usefultools.controllers;

import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.jsonObj.ShiftFindCalEvents;
import com.thegarver.usefultools.service.UserSettingsService;
import com.thegarver.usefultools.service.UsersService;
import com.thegarver.usefultools.util.DateUtility;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ShiftFinderController {

    private final String CURRENT_PAGE;
    private final String SHIFTSELECTION_REDIRECT;
    private final SimpleDateFormat DATE_ONLY_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private int WEEKS_INTO_CYCLE;

    private DateUtility OLD_SCHED_END_DATE;
    {
        try {
            OLD_SCHED_END_DATE = new DateUtility(DATE_ONLY_FORMAT.parse("2020-10-31"), true, false);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private DateUtility TODAYS_DATE = new DateUtility(true, true);

    private UsersService usersService;
    private UserSettingsService userSettingsService;

    private static final Logger log = LoggerFactory.getLogger(ShiftFinderController.class);

    @Autowired
    public ShiftFinderController(@Value("${pageLinks.shiftFinder}") String page,
                                 @Value("redirect:/${pageLinks.shiftSelection}") String shiftSelectionPage,
                                 UsersService usersService,
                                 UserSettingsService userSettingsService) {
        this.SHIFTSELECTION_REDIRECT = shiftSelectionPage;
        this.CURRENT_PAGE = page;
        this.usersService = usersService;
        this.userSettingsService = userSettingsService;
    }

    @GetMapping("/${pageLinks.shiftFinder}")
    public String openShiftFinder(Model model,
                                  @CookieValue(value = GlobalStaticVars.COOKIE_SF_START_CODE, defaultValue = "") String cookieStartCode,
                                  HttpServletResponse response,
                                  Principal principal) throws ParseException {

        int parsedStartCodeCookie;

        try {
            parsedStartCodeCookie = Integer.parseInt(cookieStartCode);
        } catch (NumberFormatException e) {
            parsedStartCodeCookie = 1;
        }
        /*
         * If no cookie is found holding a shift code it'll direct the user to the shift selection page.
         * On there they select
         * TUE - SAT (code 1)
         * MON - FRI (code 2)
         * SUN - THU (code 3)
         *
         * Once they select the shift, this will then go into calculating their start code
         * This is the code they had for the first week of the new shifted schedule.
         */
        int startCode;
        if (principal == null) {
            if (cookieStartCode.isEmpty()) {
                return SHIFTSELECTION_REDIRECT;
            }
            startCode = parsedStartCodeCookie;
        } else {
            Users users = usersService.findUserFromEmail(principal.getName());
            int userSfStartCode = users.getUserSettings().getSfStartCode();

            if (!cookieStartCode.isEmpty() && (parsedStartCodeCookie != userSfStartCode)) {
                userSettingsService.updateSfStartCode(users, parsedStartCodeCookie);
                startCode = parsedStartCodeCookie;
            } else {
                startCode = userSfStartCode;
            }
        }

        int weeksTillToday = OLD_SCHED_END_DATE.weeksTillDate(TODAYS_DATE);

        /*
         * This section assigns the weeks into your cycle on 11/1/2020
         * If you selected code 3 then your 5 weeks into the cycle
         * If you selected code 2 then your 3 weeks into the cycle
         */
        int weeksIntoCycleAtStart = 1;

        if (startCode == 2) {
            weeksIntoCycleAtStart = 3;
        } else if (startCode == 3) {
            weeksIntoCycleAtStart = 5;
        }

        WEEKS_INTO_CYCLE = weeksIntoCycleAtStart;

        //have to always find from start due to uncertainty if this week is the first or second week of shift type.
        int currentCode = findCurrentCodeFromStartCode(startCode, weeksTillToday);

        int dateCode = getCodeForFirstOfMonth(currentCode, TODAYS_DATE.getWeekOfMonthNumber());

        model.addAttribute("currentDate", DATE_ONLY_FORMAT.format(new Date()));
        model.addAttribute("calendarData", getWeekStartAndEndDates(dateCode));
        return CURRENT_PAGE;
    }

    @PostMapping("/${pageLinks.shiftFinder}/setcode")
    public String changeShiftSelection(@RequestParam("currentcode") String currentCode, HttpServletResponse response, Principal principal) {
        int weeksTillToday = OLD_SCHED_END_DATE.weeksTillDate(TODAYS_DATE);
        int startCode = calculateStartCode(response, weeksTillToday, Integer.parseInt(currentCode));

        if (principal != null) {
            Users users = usersService.findUserFromEmail(principal.getName());

            userSettingsService.updateSfStartCode(users, startCode);
        }

        return GlobalStaticVars.RETURN_REDIRECT + CURRENT_PAGE;
    }

    private int calculateStartCode(HttpServletResponse response, int weeksTillToday, int currentCode) {
        int temp = currentCode;
        for (int i = weeksTillToday; i > 1; i--) {
            if (i % 2 != 0) temp = temp == 1 ? 3 : temp - 1;
        }

        Cookie cookie = new Cookie(GlobalStaticVars.COOKIE_SF_START_CODE, Integer.toString(temp));
        cookie.setMaxAge(60*60*24*365); //one year.
        response.addCookie(cookie);

        return temp;
    }

    private int findCurrentCodeFromStartCode(int startCode, int weeksTillToday) {
        int temp = startCode;
        for (int i = 1; i < weeksTillToday; i++) {
            if (WEEKS_INTO_CYCLE % 2 == 0) temp = temp == 3 ? 1 : temp + 1;
            WEEKS_INTO_CYCLE = WEEKS_INTO_CYCLE == 6 ? 1 : WEEKS_INTO_CYCLE + 1;
        }
        return temp;
    }

    private int getCodeForFirstOfMonth(int currentCode, int weekOfMonthNumber) {
        int temp = currentCode;
        for (int i = weekOfMonthNumber; i > 1; i--) {
            if (WEEKS_INTO_CYCLE % 2 != 0) temp = temp == 1 ? 3 : temp - 1;
            WEEKS_INTO_CYCLE = WEEKS_INTO_CYCLE == 1 ? 6 : WEEKS_INTO_CYCLE - 1;
        }
        return temp;
    }

    private List<ShiftFindCalEvents> getWeekStartAndEndDates(int dateCode) {
        DateUtility firstOfTheMonth = new DateUtility(true, true);
        firstOfTheMonth.adjustDateByDayCount(1-firstOfTheMonth.getDayOfMonth());

        String lightModeBackground = "#008966";
        String darkModeBackground = "#1e7e34";

        List<ShiftFindCalEvents> events = new ArrayList<>();

        //104 weeks - aka 2 years
        for (int i = 0; i < 104; i++) {
            String startDate = "";
            String endDate = "";

            String payDay = DATE_ONLY_FORMAT.format(firstOfTheMonth.getDateFromWeekDay(DateUtility.FRIDAY));
            if (WEEKS_INTO_CYCLE % 2 != 0) events.add(new ShiftFindCalEvents.ShiftFindCalEventWithBgColor(payDay, payDay, "Pay Day", lightModeBackground));

            switch (dateCode) {
                case 1:
                    startDate = DATE_ONLY_FORMAT.format(firstOfTheMonth.getDateFromWeekDay(DateUtility.TUESDAY));
                    firstOfTheMonth.adjustDateByDayCount(7); //shifting to next week to get that sundays date since sunday starts the week
                    endDate = DATE_ONLY_FORMAT.format(firstOfTheMonth.getDateFromWeekDay(DateUtility.SUNDAY));
                    break;
                case 2:
                    startDate = DATE_ONLY_FORMAT.format(firstOfTheMonth.getDateFromWeekDay(DateUtility.MONDAY));
                    endDate = DATE_ONLY_FORMAT.format(firstOfTheMonth.getDateFromWeekDay(DateUtility.SATURDAY));
                    break;
                case 3:
                    startDate = DATE_ONLY_FORMAT.format(firstOfTheMonth.getDateFromWeekDay(DateUtility.SUNDAY));
                    endDate = DATE_ONLY_FORMAT.format(firstOfTheMonth.getDateFromWeekDay(DateUtility.FRIDAY));
                    break;
            }
            events.add(new ShiftFindCalEvents(startDate, endDate));
            if (dateCode != 1) firstOfTheMonth.adjustDateByDayCount(7); //shifting to the next week if not shift code 1 since it already shifted

            //this says if the weeks into cycle is even then change the shiftID otherwise don't to allow double weeks
            if (WEEKS_INTO_CYCLE % 2 == 0) {
                dateCode = dateCode == 3 ? 1 : dateCode + 1;
            }
            WEEKS_INTO_CYCLE = WEEKS_INTO_CYCLE == 6 ? 1 : WEEKS_INTO_CYCLE + 1;
        }

        return events;
    }
}
