package com.thegarver.usefultools.controllers;

import com.thegarver.usefultools.service.UserSettingsService;
import com.thegarver.usefultools.service.UsersService;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
public class HomepageController {

    @Autowired
    UsersService usersService;

    @GetMapping("/")
    public String displayHomePage(Model model, HttpServletResponse response, Principal principal) {

        try {
            if (!principal.getName().equals("")) {
                String darkModeEnabled = "false";
                if (usersService.findUserFromEmail(principal.getName()).getUserSettings().isDarkMode()) darkModeEnabled = "true";
                Cookie cookie = new Cookie(GlobalStaticVars.COOKIE_DARKMODE_NAME, darkModeEnabled);
                cookie.setMaxAge(60 * 60 * 24 * 52);//one year
                response.addCookie(cookie);
            }
        } catch (NullPointerException nullPointerException) {
            //means this is an anonymous user.
        }

        List<String> lines = new ArrayList<>();
        InputStream input = getClass().getResourceAsStream("/static/TXT/RonSwansonQuotes.txt");

        BufferedReader r = new BufferedReader(new InputStreamReader(input));
        try {
            String line;
            while ((line=r.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Random random = new Random();

        String quote = lines.get(random.nextInt(lines.size()));
        quote = quote + " - Ron Swanson";

        model.addAttribute("randomQuote", quote);
        return "homepage";
    }
}
