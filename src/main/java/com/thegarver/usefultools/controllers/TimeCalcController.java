package com.thegarver.usefultools.controllers;

import com.thegarver.usefultools.dao.TcTimes;
import com.thegarver.usefultools.dao.UserSettings;
import com.thegarver.usefultools.dao.Users;
import com.thegarver.usefultools.service.UserSettingsService;
import com.thegarver.usefultools.service.UsersService;
import com.thegarver.usefultools.userInput.ClockInOut;
import com.thegarver.usefultools.service.TcTimeService;
import com.thegarver.usefultools.util.DateUtility;
import com.thegarver.usefultools.util.GlobalStaticVars;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class TimeCalcController {

    private final String CURRENT_PAGE;
    private final String RELOAD_PAGE;

    //Format info into ClockInOut objects and pass to submission function.
    private final SimpleDateFormat dateOnlyFormat = new SimpleDateFormat("MM/dd/yyyy");
    private final TcTimeService tcTimeService;
    private final UsersService usersService;
    private final UserSettingsService userSettingsService;

    private static final Logger log = LoggerFactory.getLogger(TimeCalcController.class);

    @Autowired
    public TimeCalcController(TcTimeService tcTimeService,
                              UsersService usersService,
                              UserSettingsService userSettingsService,
                              @Value("${pageLinks.timeCalc}") String pageName) {
        this.tcTimeService = tcTimeService;
        this.usersService = usersService;
        this.userSettingsService = userSettingsService;
        this.CURRENT_PAGE = pageName;
        this.RELOAD_PAGE = GlobalStaticVars.RETURN_REDIRECT + pageName; //this is referenced in timecalc.html
    }

    @GetMapping("/${pageLinks.timeCalc}")
    public String displayTimeCalcPage(Principal principal, HttpServletResponse response, Model model)
    {
        return buildPageAndReturn(usersService.findUserFromEmail(principal.getName()), model);
    }

    @PostMapping("/${pageLinks.timeCalc}/manual")
    @ResponseBody
    public String receivedManualTimeInput(Principal principal, @RequestBody List<ClockInOut> data,
                                          HttpServletResponse response, Model model) {

        formatTimeAndSubmit(usersService.findUserFromEmail(principal.getName()), data);
        return "Manual Post Complete.";
    }

    @GetMapping("/${pageLinks.timeCalc}/reset")
    private String clearUserTimes(Principal principal, HttpServletResponse response) {
        Users users = usersService.findUserFromEmail(principal.getName());

        tcTimeService.deleteUserRecords(users);
        return RELOAD_PAGE;
    }

    @PostMapping("/${pageLinks.timeCalc}/wagehourupdate")
    @ResponseBody
    private String updateWageAndInfo(Principal principal, @RequestBody String data) {

        //todo add this to method parameters if it has issues - HttpServletResponse response, Model model
        String[] values = data.split(",");
        double wage = TcTimeService.DEFAULT_WAGE;
        int hours = TcTimeService.DEFAULT_HOURS;
        try {
            wage = Double.parseDouble(values[0]);
            hours = Integer.parseInt(values[1]);
        } catch (NumberFormatException e) {
            e.printStackTrace(); //this should never happen.
        }
        userSettingsService.updateWageAndHours(usersService.findUserFromEmail(principal.getName()), wage, hours);
        return "Wage and Hours Updated for " + principal.getName();
    }

    @PostMapping("/${pageLinks.timeCalc}/infoseen")
    @ResponseBody
    private String updateInfoSeen(Principal principal) {

        userSettingsService.updateInfoSeen(usersService.findUserFromEmail(principal.getName()));

        return "Info Seen updated to true";
    }

    private String buildPageAndReturn(Users users, Model model) {
        UserSettings userSettings = users.getUserSettings();

        DateUtility dateUtility = new DateUtility(true, true);

        List<TcTimes> tcTimesForUser = tcTimeService.getAllTimesForID(users);

        List<ClockInOut> convertedTimes = new ArrayList<>();
        int workedHours = 0;
        int workedRemainingMinutes = 0;
        if (tcTimesForUser.size() != 0) {
            long workedMilliseconds = 0;
            for (TcTimes tcTimes : tcTimesForUser) {
                convertedTimes.add(tcTimes.convertToUserInput());
                workedMilliseconds = workedMilliseconds + tcTimes.getMillisecondsWorked();
            }
            int workedMinutes = (int) workedMilliseconds / 1000 / 60;
            workedHours = workedMinutes / 60;
            workedRemainingMinutes = workedMinutes % 60;
        }

        ArrayList<Integer> days = dateUtility.getThisWeeksDayNums();

        model.addAttribute("tcHoursID", "timeCalcHours");
        model.addAttribute("tcWageID", "timeCalcWage");
        model.addAttribute("hourSpecified", userSettings.getTcHours());
        model.addAttribute("wageSpecified", userSettings.getTcWage());
        model.addAttribute("infoSeen", userSettings.isInfoSeen());
        model.addAttribute("hoursWorked", workedHours);
        model.addAttribute("minutesWorked", workedRemainingMinutes);
        model.addAttribute("savedShifts", convertedTimes);

        model.addAttribute("sundayDay", "Sunday - " + days.get(0));
        model.addAttribute("mondayDay", "Monday - " + days.get(1));
        model.addAttribute("tuesdayDay", "Tuesday - " + days.get(2));
        model.addAttribute("wednesdayDay", "Wednesday - " + days.get(3));
        model.addAttribute("thursdayDay", "Thursday - " + days.get(4));
        model.addAttribute("fridayDay", "Friday - " + days.get(5));
        model.addAttribute("saturdayDay", "Saturday - " + days.get(6));
        model.addAttribute("sundayDate", dateOnlyFormat.format(dateUtility.getDateFromWeekDay(0)));
        model.addAttribute("saturdayDate", dateOnlyFormat.format(dateUtility.getDateFromWeekDay(6)));

        return CURRENT_PAGE;
    }

    private void formatTimeAndSubmit(Users users, List<ClockInOut> clockInOut) {
        SimpleDateFormat fullDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String staticDate = "01/01/2020 "; //extra space is intentional and date needs to match date in timecalc.html

        tcTimeService.deleteUserRecords(users);

        for (ClockInOut userInput : clockInOut) {

            Date timeIn = null;
            Date timeOut = null;
            try {
                if (!userInput.getClockIn().equals("")) {
                    timeIn = fullDateTimeFormat.parse(staticDate + userInput.getClockIn());
                }
                if (!userInput.getClockOut().equals("")) {
                    timeOut = fullDateTimeFormat.parse(staticDate + userInput.getClockOut());
                }
            } catch (ParseException ignored) {
                log.error("Failure to parse time in ClockInOut class");
            }
            tcTimeService.addTimes(users, userInput.getShiftCount(), userInput.getDay(), timeIn, timeOut);
        }
    }
}
