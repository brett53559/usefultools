package com.thegarver.usefultools;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class UsefulToolsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsefulToolsApplication.class, args);
    }

    @Bean
    public ServletWebServerFactory servletContainer() {
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint securityConstraint = new SecurityConstraint();
                securityConstraint.setUserConstraint("CONFIDENTIAL");
                SecurityCollection collection = new SecurityCollection();
                collection.addPattern("/*");
                securityConstraint.addCollection(collection);
                context.addConstraint(securityConstraint);
            }
        };
        tomcat.addAdditionalTomcatConnectors(redirectConnector());
        return tomcat;
    }

    private Connector redirectConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        connector.setPort(80);
        connector.setSecure(false);
        connector.setRedirectPort(443);
        return connector;
    }

    /**
     * TODO List IMMEDIATE
     * Optimize darkmode
     *      Get navbar and footer to use different settings
     *      Figure out why timecalc and shift finder look like ass in darkmode.
     * Create a way to quickly disable a page if it's found it needs work.
     * Create a review submission section and incorporate the Azure Content Moderation
     *      Check images for racy/adult content and then check image for text
     *      Analyze that text with the review text for profane language.
     *      Email a response of either
     *          Your review has been accepted and is under review
     *          Your review contained profane language and will not be accepted
     *          Your review was submitted with an image that contained racy/adult/profane content
     * Create a page accessible by administrators to analyze review submissions for approval.
     * Change all the field validations to use one javascript method (has-error, has-success)
     * Create more schedule variations and allow the user to select theirs and store via cookie.
     */

    /**
     * TODO Eventually
     * update all html files to use ${@environment.getProperty('css.specific.name')} instead of hardcoded url paths
     * Profile page
     *      Email activation link when doing an email change.
     */
}
