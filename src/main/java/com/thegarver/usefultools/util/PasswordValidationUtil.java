package com.thegarver.usefultools.util;

import com.thegarver.usefultools.dto.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PasswordValidationUtil {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    //Password matching is already done client side and only one password is submitted back.
    public boolean validatePassword(Model model, UserInfo userInfo) {
        if (passwordHasBeenPwned(userInfo.getPassword())) return false;

        //If passwords match or contain parts of the user's name
        if (userInfo.getPassword().length() < 12 ||
                userInfo.getPassword().toLowerCase().contains(userInfo.getFirstName().toLowerCase()) ||
                userInfo.getPassword().toLowerCase().contains(userInfo.getLastName().toLowerCase())) {
            return false;
        }

        return true;
    }
    
    private boolean passwordHasBeenPwned(String pass) {
        //Verify that the password provided isn't in the "haveibeenpwned" database
        StringHasher hashedPassword = new StringHasher(pass);
        String sha1Password = hashedPassword.getSha1().toUpperCase();
        try {
            URL url = new URL("https://api.pwnedpasswords.com/range/" + sha1Password.substring(0,5));
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.contains(sha1Password.substring(5))) {
                    return true;
                }
            }
            in.close();
            con.disconnect();
        } catch (Exception e) {LOGGER.error(e.getMessage());}
        return false;
    }
}
