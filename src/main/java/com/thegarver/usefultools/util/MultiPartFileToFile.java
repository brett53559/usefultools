package com.thegarver.usefultools.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class MultiPartFileToFile {

    private File convertedFile;

    public MultiPartFileToFile(MultipartFile multipartFile) {
        convertedFile = convertToFile(multipartFile);
    }

    private File convertToFile(MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();

            String osBasePath = "";
            String dirName = "TempSpringFiles";
            String osName = System.getProperty("os.name").toLowerCase(); //Query the system name to create correct save path
            if (osName.contains("win")) {
                osBasePath = "C:\\" + dirName;
            }else if (osName.contains("nux")) {
                osBasePath = "/home/" + dirName;
            }
            // Creating the directory to store file
            File dir = new File(osBasePath + File.separator);
            if (!dir.exists())
                dir.mkdirs();

            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath()
                    + File.separator + file.getOriginalFilename());
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();

            return serverFile;
        } catch (Exception e) {
            return null;
        }
    }

    public File getConvertedFile() {
        return convertedFile;
    }
}
