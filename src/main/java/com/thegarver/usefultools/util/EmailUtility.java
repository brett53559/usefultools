//package com.thegarver.usefultools.util;
//
//import com.thegarver.usefultools.service.EmailService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//
//@Service
//public class EmailUtility {
//    private final EmailService emailService;
//
//    private final String REGISTRATION_PAGE;
//    private final String PASS_RESET_PAGE;
//    private final String DOMAIN_NAME;
//
//    @Autowired
//    public EmailUtility(EmailService emailService,
//                        @Value("${pageLinks.register}") String registrationPage,
//                        @Value("${pageLinks.passreset}") String passResetPage,
//                        @Value("${domain.current}") String domainName) {
//        this.emailService = emailService;
//        this.REGISTRATION_PAGE = registrationPage;
//        this.PASS_RESET_PAGE = passResetPage;
//        this.DOMAIN_NAME = domainName;
//    }
//
//    public void sendErrorEmail(String errorString) {
//        emailService.sendSimpleMessage("bgarver@outlook.com", "Error within Useful Tools", errorString);
//    }
//
//    public void sendWelcomeEmail(String toEmail, String username, String registrationId) {
//        String subject = "Welcome to Garver's Useful Tools";
//        String registrationUrl = "https://" + DOMAIN_NAME + "/" + REGISTRATION_PAGE + "/activate?" + GlobalStaticVars.URL_PARAM_ACTIVATIONID + "=" + registrationId;
//        try {
//            byte[] encoded = Files.readAllBytes(Paths.get("src",  "main", "resources", "static", "Email", "welcomeemail.html"));
//            String htmlBody = new String(encoded, Charset.defaultCharset());
//            htmlBody = htmlBody.replace("ABCINSERTEDUSERNAMEABC", username);
//            htmlBody = htmlBody.replace("ABCINSERTEDURLABC", registrationUrl);
//            String textBody = "Thank you for registering an account with thegarver.com Please finalize your registration.\n" +
//                    "\n" +
//                    "Confirm your registration!\n" +
//                    "\n" +
//                    "Thank you for registering an account with\n" +
//                    "\n" +
//                    "Useful Tools\n" +
//                    "\n" +
//                    "Before you may use the site, you must first confirm you email address. To do so, please click the link below.\n" +
//                    "\n" +
//                    "Click here to finish activation. [" + registrationUrl + "]\n" +
//                    "\n" +
//                    "This link will expire in 48 hours!\n" +
//                    "\n" +
//                    "If the above link doesn't work, copy and paste the link below into your browser's URL.\n" +
//                    "\n" +
//                     registrationUrl + "\n" +
//                    "\n" +
//                    "Useful Tools\n" +
//                    "\n" +
//                    "1234 State St. Kearney, Nebraska 68845 United States You are receiving this email because you, or someone used this email to register an account with thegarver.com\n" +
//                    "\n" +
//                    "If you did not attempt to register an account, your email address may have been provided by accident, or by malicious intent. Please consider changing your email password.";
//            emailService.sendHtmlMessage(toEmail, subject, textBody, htmlBody);
//        } catch (Exception e) {}
//    }
//
//    public void sendPassResetEmail(String toEmail, String name, String resetId) {
//        String subject = "Forgot your password huh?";
//        String resetUrl = DOMAIN_NAME + "/" + PASS_RESET_PAGE + "?" + GlobalStaticVars.URL_PARAM_PASSRESETID + "=" + resetId;
//        try {
//            byte[] encoded = Files.readAllBytes(Paths.get("src",  "main", "resources", "static", "Email", "passreset.html"));
//            String htmlBody = new String(encoded, Charset.defaultCharset());
//            htmlBody = htmlBody.replace("ABCUSERNAMEABC", name);
//            htmlBody = htmlBody.replace("ABCEMAILABC", toEmail);
//            htmlBody = htmlBody.replace("ABCLINKABC", resetUrl);
//            String textBody = "We've received a request to reset your password for the Avi Ranch account associate with " + toEmail + "\n" +
//                    "At this time, no changes have been made to your account.\n\n" +
//                    "You can reset your password by clicking this link: " + resetUrl + "\n\n" +
//                    "If you did not request a new password, please let us know immediately by replying to this email.\n" +
//                    "-Avi Ranch team.";
//            emailService.sendHtmlMessage(toEmail, subject, textBody, htmlBody);
//        } catch (Exception e) {}
//    }
//}
