package com.thegarver.usefultools.util;

public class GlobalStaticVars {
    //Cookies
    public static final String COOKIE_DARKMODE_NAME = "darkmode";
    public static final String COOKIE_SF_START_CODE = "sf.startcode";

    //parameters for login page
    public static final String URL_PARAM_AUTHENTICATIONERROR = "authenticationerror";
    public static final String URL_PARAM_BADLOGIN = "badlogin";
    public static final String URL_PARAM_USERDISABLED = "userdisabled";
    public static final String URL_PARAM_USERACTIVATED = "useractivated";
    public static final String URL_PARAM_NEWUSER = "newuser";
    public static final String URL_PARAM_FORGOTPASS = "forgotpass";
    public static final String URL_PARAM_PASSRESET = "passreset";

    //parameters for profile page
    public static final String URL_PARAM_OLDPASSINCORRECT = "oldpassincorrect";
    public static final String URL_PARAM_PASSUPDATED = "passwordupdated";
    public static final String URL_PARAM_NAMEUPDATED = "nameupdated";
    public static final String URL_PARAM_INVALIDPASS = "passnotvalid";

    //parameters for registration page
    public static final String URL_PARAM_ACTIVATIONIDINVALID = "activationidinvalid";
    public static final String URL_PARAM_EMAILTAKEN = "emailtaken";
    public static final String URL_PARAM_ACTIVATIONID = "activationid";

    //parameters for password reset controller
    public static final String URL_PARAM_PASSRESETID = "passresetid";

    //parameters for home page
    public static final String URL_PARAM_LOGOUT = "logout";

    public static final String DOMAIN_NAME = "https://thegarver.com";

    public static final String RETURN_REDIRECT = "redirect:/";
}
