package com.thegarver.usefultools.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class StringHasher {

    private String unEncodedString;

    public StringHasher(String string) {
        unEncodedString = string;
    }

    private String hashTheString(String hashType) {
        byte[] encodedHash = null;

        try {
            MessageDigest digest = MessageDigest.getInstance(hashType);
            encodedHash = digest.digest(
                    unEncodedString.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {}

        StringBuilder hexString = new StringBuilder(2 * encodedHash.length);
        for (int i = 0; i < encodedHash.length; i++) {
            String hex = Integer.toHexString(0xff & encodedHash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public String getSha256() {
        return hashTheString("SHA-256");
    }

    public String getSha1() {
        return hashTheString("SHA-1");
    }


}
