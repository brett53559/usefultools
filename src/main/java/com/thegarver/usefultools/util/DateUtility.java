package com.thegarver.usefultools.util;

import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtility {
    //The default action for Calendar is to have Sunday be 1, but i'm manually changing that to 0 by subtracting one
    public final static int SUNDAY = 0;
    public final static int MONDAY = 1;
    public final static int TUESDAY = 2;
    public final static int WEDNESDAY = 3;
    public final static int THURSDAY = 4;
    public final static int FRIDAY = 5;
    public final static int SATURDAY = 6;

    //Difference from AWS system time to your actual time
    private int timeDiffHour = -6;
    private int timeDiffMinute = 0;

    private Calendar calendar = Calendar.getInstance();

    public DateUtility(boolean setTimeToZero, boolean UTCtoCST) {
        setDate(new Date(), setTimeToZero, UTCtoCST);
    }

    public DateUtility(Date date, boolean setTimeToZero, boolean UTCtoCST) {
        setDate(date, setTimeToZero, UTCtoCST);
    }

    private void setDate(Date date, boolean setTimeToZero, boolean UTCtoCST) {
        calendar.setTime(date);
        if (UTCtoCST) {
            calendar.add(Calendar.HOUR, timeDiffHour);
            calendar.add(Calendar.MINUTE, timeDiffMinute);
        }
        if (setTimeToZero) {
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
        }
    }

    public Date getCurrentDate() {
        return calendar.getTime();
    }

    public Date getDateFromWeekDay(int dayNum) {
        Date permDate = calendar.getTime();
        switch (dayNum) {
            case 0:
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                break;
            case 1:
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                break;
            case 2:
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
                break;
            case 3:
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
                break;
            case 4:
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
                break;
            case 5:
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
                break;
            case 6:
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                break;
        }
        Date temp = calendar.getTime();
        calendar.setTime(permDate);
        return temp;
    }

    public int getWeekDayNumber() {
        return calendar.get(Calendar.DAY_OF_WEEK) - 1;
    }

    public ArrayList<Integer> getThisWeeksDayNums() {
        ArrayList<Integer> dayNums = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            DateUtility dateUtility = new DateUtility(getDateFromWeekDay(i), true, false);
            dayNums.add(dateUtility.getDayOfMonth());
        }
        return dayNums;
    }

    public int getDayOfYearNumber() {
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    public int getDayOfMonth() {
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public int getWeekOfMonthNumber() {
        return calendar.get(Calendar.WEEK_OF_MONTH);
    }

    public int getWeekOfYearNumber() {
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    public int getYear() {
        return calendar.get(Calendar.YEAR);
    }

    public int getMonthNum() {
        return calendar.get(Calendar.MONTH) + 1;
    }

    //make sure to pass the newer date in and not the older date.
    public int daysTillDate(DateUtility dateUtility) {
        int dayCount = 0;
        Date tempdate = calendar.getTime();
        while (
                dateUtility.getYear() != getYear() ||
                dateUtility.getDayOfYearNumber() != getDayOfYearNumber()) {
            dayCount++;
            adjustDateByDayCount(1);
        }
        calendar.setTime(tempdate);
        return dayCount;
    }

    public int weeksTillDate(DateUtility dateUtility) {
        int weekCount = 0;
        Date tempdate = calendar.getTime();

        while (true) {
            boolean searchingForDate = true;
            for (int i = 0; i < 7; i++) {
                if (dateUtility.toString().equals(getDateFromWeekDay(i).toString())) {
                    searchingForDate = false;
                }
            }
            if (!searchingForDate) {
                break;
            }
            weekCount++;
            calendar.add(Calendar.WEEK_OF_YEAR, 1);
        }
        calendar.setTime(tempdate);
        return weekCount;
    }

    public void adjustDateByDayCount(int dayCount) {
        calendar.add(Calendar.DAY_OF_MONTH, dayCount);
    }

    public void adjustDateByWeekCount(int weekCount) {
        calendar.add(Calendar.WEEK_OF_YEAR, weekCount);
    }

    @Override
    public String toString() {
        return calendar.getTime().toString();
    }
}
