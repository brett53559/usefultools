package com.thegarver.usefultools.userInput;

public class ClockInOut {
    private int day;
    private int shiftCount;
    private String clockIn;
    private String clockOut;

    //ClockIn and ClockOuts have to be string in HH:mm format because that's the only format the <input type="time"> tags will accept
    public ClockInOut() {
    }

    public ClockInOut(int day, int shiftCount, String clockIn, String clockOut) {
        this.day = day;
        this.shiftCount = shiftCount;
        this.clockIn = clockIn;
        this.clockOut = clockOut;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getShiftCount() {
        return shiftCount;
    }

    public void setShiftCount(int shiftCount) {
        this.shiftCount = shiftCount;
    }

    public String getClockIn() {
        return clockIn;
    }

    public void setClockIn(String clockIn) {
        this.clockIn = clockIn;
    }

    public String getClockOut() {
        return clockOut;
    }

    public void setClockOut(String clockOut) {
        this.clockOut = clockOut;
    }
}
