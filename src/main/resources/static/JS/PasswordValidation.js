//Password variables
var div1 = document.getElementById("divNewPass");
var div2 = document.getElementById("divRepeatPass");
var pass1 = document.getElementById("newpass");
var pass2 = document.getElementById("repeatpass");
var feedback1 = document.getElementById("divNewPassFeedback");
var feedback2 = document.getElementById("divRepeatPassFeedback");

function activePasswordValidation() {
    if (pass1.value === pass2.value) {
        div1.classList.remove("has-danger");
        div1.classList.add("has-success");
        div2.classList.remove("has-danger");
        div2.classList.add("has-success");
        pass1.classList.remove("is-invalid");
        pass1.classList.add("is-valid");
        pass2.classList.remove("is-invalid");
        pass2.classList.add("is-valid");
        feedback2.classList.remove("invalid-feedback");
        feedback2.classList.add("valid-feedback");
        feedback2.innerHTML = "Passwords match!";
    } else
    {
        div1.classList.add("has-danger");
        div1.classList.remove("has-success");
        div2.classList.add("has-danger");
        div2.classList.remove("has-success");
        pass1.classList.add("is-invalid");
        pass1.classList.remove("is-valid");
        pass2.classList.add("is-invalid");
        pass2.classList.remove("is-valid");
        feedback2.classList.add("invalid-feedback");
        feedback2.classList.remove("valid-feedback");
        feedback2.innerHTML = "Passwords don't match!";
    }
}