onmessage = function(e) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/timecalc/manual", false);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    // send the collected data as JSON
    xhr.addEventListener("loadend", function (e) {
        console.log("start of data");
        console.log(e);
        console.log("end of data");
        postMessage({result: true});
    });
    xhr.send(JSON.stringify(e.data));
};