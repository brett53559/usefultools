//Emails variables
var newEmailDivTag = document.getElementById("divEmail");
var newEmailInputTag = document.getElementById("newemail");
var newEmailFeedbackTag = document.getElementById("divFeedbackEmail");

function emailValidation(email) {
    if (emailIsValid(email))
    {
        setEmailFeedback(true, "Email format is valid!");
    } else {
        setEmailFeedback(false, "Email input is invalid. Please try again.")
    }
}

function setEmailFeedback(valid, message) {
    if (valid === true) {
        newEmailDivTag.classList.remove("has-danger");
        newEmailDivTag.classList.add("has-success");
        newEmailInputTag.classList.remove("is-invalid");
        newEmailInputTag.classList.add("is-valid");
        newEmailFeedbackTag.classList.remove("invalid-feedback");
        newEmailFeedbackTag.classList.add("valid-feedback");
        newEmailFeedbackTag.innerHTML = message;
    } else {
        newEmailDivTag.classList.add("has-danger");
        newEmailDivTag.classList.remove("has-success");
        newEmailInputTag.classList.add("is-invalid");
        newEmailInputTag.classList.remove("is-valid");
        newEmailFeedbackTag.classList.add("invalid-feedback");
        newEmailFeedbackTag.classList.remove("valid-feedback");
        newEmailFeedbackTag.innerHTML = message;
    }
}

function emailIsValid(email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) //Referenced from https://www.w3resource.com/javascript/form/email-validation.php
    {
        return true;
    }
    return false;
}